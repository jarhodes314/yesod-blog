{-# LANGUAGE TemplateHaskell #-}
module CustomFields.Permission 
    {-( module CustomFields 
    )-} where

import Database.Persist.TH

data Permission 
    = Admin
    deriving (Show, Read, Eq)
derivePersistField "Permission"
