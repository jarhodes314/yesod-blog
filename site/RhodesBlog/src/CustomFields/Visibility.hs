{-# LANGUAGE TemplateHaskell #-}
module CustomFields.Visibility where

import Database.Persist.TH

data Visibility
    = Visible 
    | Hidden
    deriving (Show, Read, Eq)
derivePersistField "Visibility"
