{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
module Handler.BlogPost where

import Import
import qualified Data.CaseInsensitive as CI
import qualified Data.Text.Encoding as TE
import Text.Julius (rawJS)
import Yesod.Core.Handler (defaultCsrfCookieName, defaultCsrfHeaderName)
import qualified Data.Text as T
import Text.Read

blogContent :: Form (Text,Html)
blogContent = renderDivs $ (,)
    <$> areq textField "" Nothing
    <*> areq htmlField "" Nothing

postVisible :: Form Text
postVisible = renderDivs $ areq textField "" Nothing

getBlogPostR :: BlogPostUrl -> Handler Html
getBlogPostR postUrl = do
    mblogpost <- runDB $ selectFirst [BlogPostUrl ==. postUrl] []
    (widget,enctype) <- generateFormPost blogContent
    (widget2,enctype2) <- generateFormPost postVisible 

    case mblogpost of
        Nothing -> notFound
        _ -> return ()
    
    let Just (Entity pid post) = mblogpost
    let BlogPost title authorId _url content authored updated active = post
    User _ident _password authorName _auth <- runDB $ get404 authorId

    let hidden = active /= Visible

    if active == Visible 
    then return ()
    else do 
        auth <- isAdmin
        case auth of
            Authorized -> return ()
            _          -> notFound

    isAd <- isAdmin
    let admin = case isAd of
                    Authorized -> True
                    _          -> False
 
    let hname = rawJS $ TE.decodeUtf8 $ CI.foldedCase defaultCsrfHeaderName
        cname = rawJS $ TE.decodeUtf8 defaultCsrfCookieName
    
    currentTime <- liftIO getCurrentTime

    let customFormatTime :: UTCTime -> String
        customFormatTime time
         | diffDays (utctDay currentTime) (utctDay time) >= 1 = formatTime defaultTimeLocale "%e %B %Y" time
         | otherwise = formatTime defaultTimeLocale "%R today" time

    defaultLayout $ do
        setTitle (toHtml title)
        if admin
        then $(widgetFile "blogpost-admin")
        else $(widgetFile "blogpost")

postBlogPostR :: BlogPostUrl -> Handler Html
postBlogPostR postUrl = do
    ((result,widget),_) <- runFormPost blogContent
    ((result2,widget2),_) <- runFormPost postVisible
    mblogpost <- runDB $ selectFirst [BlogPostUrl ==. postUrl] []

    isAd <- isAdmin
    let admin = case isAd of
                    Authorized -> True
                    _          -> False
    case isAd of
        Authorized -> return ()
        _ -> notFound

    case mblogpost of
        Nothing -> notFound
        _ -> return ()

    let Just (Entity pid post) = mblogpost
    let BlogPost title authorId _url content authored updated active = post
    let hidden = active /= Visible

    User _ident _password authorName _auth <- runDB $ get404 authorId

    currentTime <- liftIO getCurrentTime

    let customFormatTime :: UTCTime -> String
        customFormatTime time
         | diffDays (utctDay currentTime) (utctDay time) >= 1 = formatTime defaultTimeLocale "%e %B %Y" time
         | otherwise = formatTime defaultTimeLocale "%R today" time

    case result of
        FormSuccess (title',content') -> do
            liftIO $ print title'
            if T.strip title' /= ""
            then runDB $ update pid [BlogPostTitle =. T.strip title']
            else return ()
             
            runDB $ update pid [BlogPostContent =. content', BlogPostUpdated =. currentTime]
        _ -> 
            case result2 of
                FormSuccess visible -> do
                    let newVis = readMaybe $ unpack visible
                    case newVis of
                        Just vis -> runDB $ update pid [BlogPostActive =. vis]
                        Nothing -> notFound
                _ -> notFound

    let hname = rawJS $ TE.decodeUtf8 $ CI.foldedCase defaultCsrfHeaderName
        cname = rawJS $ TE.decodeUtf8 defaultCsrfCookieName

    defaultLayout $ do
        setTitle (toHtml title)
        if admin
        then $(widgetFile "blogpost-admin")
        else $(widgetFile "blogpost")
