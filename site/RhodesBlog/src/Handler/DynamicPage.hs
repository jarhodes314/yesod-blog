{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
module Handler.DynamicPage where

import Import

getDynamicPageR :: DynamicPageUrl -> Handler Html
getDynamicPageR pageUrl = do
    mdynpage <- runDB $ selectFirst [DynamicPageUrl ==. pageUrl] []

    case mdynpage of
        Nothing -> notFound
        _ -> return ()
    
    let Just (Entity _dynpageId dynpage) = mdynpage
        title = dynamicPageTitle dynpage
        content = dynamicPageContent dynpage
        active = dynamicPageActive dynpage
    let _mparent = dynamicPageParent dynpage
    
    if active == Visible 
    then return ()
    else do 
        auth <- isAdmin
        case auth of
            Authorized -> return ()
            _          -> notFound
    defaultLayout $ do
        setTitle (toHtml title)
       -- addStylesheet $ StaticR $ StaticFile ["contenttools","contenttools.min.css"] []
       -- addScript $ StaticR $ StaticFile ["contenttools","contenttools.min.js"] []
       -- addScript $ StaticR contenttools_contenttools_min_js
       -- $(widgetFile "dynamicpage")
        $(widgetFile "contenttools")

postDynamicPageR :: DynamicPageUrl -> Handler Html
postDynamicPageR pageUrl = getDynamicPageR pageUrl
