{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
module Handler.Profile where

import Import
import Data.Maybe (fromJust)

data ProfileUpdate = ProfileUpdate
    { personLogin     :: Text
    , personNickname  :: Text
    , personPassword  :: Maybe Text
    , confirmPassword :: Maybe Text
    }
    deriving Show

profileForm :: User -> Html -> MForm Handler (FormResult ProfileUpdate, Widget)
profileForm current = renderDivs $ ProfileUpdate
    <$> areq textField "Login name" (Just $ userIdent current)
    <*> areq textField "Display name" (Just $ userNickname current)
    <*> aopt passwordField "New password" Nothing
    <*> aopt passwordField "Confirm password" Nothing

getProfileR :: Handler Html
getProfileR = do
    (_, user) <- requireAuthPair
    -- generate form to be displayed
    (widget, enctype) <- generateFormPost (profileForm user)
    defaultLayout $ do
        setTitle . toHtml $ userNickname user <> "'s User page"
        $(widgetFile "profile")

postProfileR :: Handler Html
postProfileR = do
    (uid, user) <- requireAuthPair
    ((result, widget), enctype) <- runFormPost (profileForm user)
    case result of
        FormSuccess (ProfileUpdate newLogin newNickname newPassword newPasswordConf) -> do
            -- check and process login name
            if newLogin /= userIdent user
            then do
                clash <- runDB $ selectFirst [UserIdent ==. newLogin] []
                case clash of
                    Nothing -> runDB (update uid [UserIdent =. newLogin]) >> (addMessage "suc" $ toHtml ("Login name successfully updated" :: Text))
                    _ -> addMessage "err" $ toHtml ("Login name \"" <> newLogin <> "\" already taken" :: Text)
            else return ()
            -- check and process nickname (display name)
            if newNickname /= userNickname user
            then do
                clash <- runDB $ selectFirst [UserNickname ==. newNickname] []
                case clash of
                    Nothing -> do
                        runDB (update uid [UserNickname =. newNickname])
                        addMessage "suc" $ toHtml ("Display name successfully updated" :: Text)
                    _ -> addMessage "err" $ toHtml ("Display name \"" <> newNickname <> "\" already taken" :: Text)
            else return ()
            -- process new password
            if newPassword /= newPasswordConf
            then addMessage "err" $ toHtml ("Passwords provided do not match" :: Text)
            else 
                if newPassword /= Nothing && newPassword /= Just ""
                then do
                    user' <- setPassword (fromJust newPassword) user
                    runDB (update uid [UserPassword =. userPassword user'])
                    addMessage "suc" $ toHtml ("Password successfully updated" :: Text)
                else return ()
            user' <- runDB $ get404 uid
            (widget, enctype) <- generateFormPost (profileForm user')
            defaultLayout $ do
                setTitle . toHtml $ userNickname user' <> "'s User page"
                $(widgetFile "profile")
        _ ->
            defaultLayout $ do
                setTitle . toHtml $ userNickname user <> "'s User page"
                $(widgetFile "profile")
