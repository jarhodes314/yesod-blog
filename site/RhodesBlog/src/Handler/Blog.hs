{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
module Handler.Blog where

import Import
import qualified Data.Text as T

newPostForm :: Html -> MForm Handler (FormResult Text, Widget)
newPostForm = renderDivs $ areq textField "New post title" Nothing

customUrlForm :: Text -> Maybe Text -> Html -> MForm Handler (FormResult (Text, Text), Widget)
customUrlForm title murl = renderDivs $ (,)
    <$> areq textField "New post title" (Just title)
    <*> areq textField "New post URL" murl

getBlogR :: Handler Html
getBlogR = do
    postsUsers <- runDB getAllPostsUsers
    isAd <- isAdmin
    let admin = case isAd of
                    Authorized -> True
                    _ -> False
    (widget, enctype) <- generateFormPost newPostForm

    currentTime <- liftIO getCurrentTime

    let customFormatTime :: UTCTime -> String
        customFormatTime time
         | diffDays (utctDay currentTime) (utctDay time) >= 1 = formatTime defaultTimeLocale "%e %B %Y" time
         | otherwise = formatTime defaultTimeLocale "%R today" time

    defaultLayout $ do
        _aDomId <- newIdent -- TODO work out what this is
        setTitle "Blog"
        $(widgetFile "blog")

postBlogR :: Handler Html
postBlogR = do
    postsUsers <- runDB getAllPostsUsers
    muid <- maybeAuthId
    isAd <- isAdmin
    time <- liftIO getCurrentTime

    currentTime <- liftIO getCurrentTime

    let customFormatTime :: UTCTime -> String
        customFormatTime time
         | diffDays (utctDay currentTime) (utctDay time) >= 1 = formatTime defaultTimeLocale "%e %B %Y" time
         | otherwise = formatTime defaultTimeLocale "%R today" time

    let admin = case isAd of
                    Authorized -> True
                    _ -> False
    if admin
    then do 
        let Just uid = muid
        ((result,widget), enctype) <- runFormPost newPostForm
        case result of
            FormSuccess name -> do
                let url = T.map (\c -> if c == ' ' then '-' else c) $ T.toLower $ T.strip $ name
                clash <- runDB $ selectFirst [BlogPostUrl ==. url] []
                case clash of
                    Nothing -> do 
                        _ <- runDB $ insert $ BlogPost
                            { blogPostTitle    = name
                            , blogPostAuthorId = uid
                            , blogPostUrl      = url
                            , blogPostContent  = ""
                            , blogPostAuthored = time
                            , blogPostUpdated  = time
                            , blogPostActive   = Hidden }
                        redirect (BlogPostR url)
                    Just _ -> do
                        (widget, enctype) <- generateFormPost (customUrlForm name (Just url))
                        addMessage "err" "Auto URL taken, pick a different one"
                        defaultLayout $ do
                            _aDomId <- newIdent
                            setTitle "Blog"
                            $(widgetFile "blog")
                        
            _ -> do
                -- TODO check other form
                (widget, enctype) <- generateFormPost newPostForm 
                defaultLayout $ do
                    _aDomId <- newIdent
                    setTitle "Blog"
                    $(widgetFile "blog")
    else do
        (widget, enctype) <- generateFormPost newPostForm 
        defaultLayout $ do
            _aDomId <- newIdent
            setTitle "Blog"
            $(widgetFile "blog")

getAllPostsUsers :: DB [(Entity BlogPost, Maybe (Entity User))]
getAllPostsUsers = let posts = selectList [] [Desc BlogPostUpdated]
                   in  posts >>= mapM (\(Entity postId post) -> selectFirst [UserId ==. blogPostAuthorId post] [] >>= \muser -> return (Entity postId post, muser))


