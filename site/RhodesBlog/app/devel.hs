{-# LANGUAGE PackageImports #-}
import "RhodesBlog" Application (develMain)
import Prelude (IO)

main :: IO ()
main = develMain
