# Yesod Bλog

This is my (soon-to-be) shiny new website. It is written in Haskell using the Yesod framework, and uses docker to run Haskell Stack, PostgreSQL and (eventually) nginx, via docker compose. It is a CMS of sorts, allowing both the creation of pages and posts and uses [ContentTools](http://getcontenttools.com/). 

## Installation and Usage

It should be pretty simple to install:
* First install Docker (and docker-compose if you are a linux user, making sure you add your user to the docker group)
* Then clone the repository and run `docker-compose up` (which may need to be run with sudo access)
* Once the containers are built, the server should start up successfully. All the stack packages and postgres data should automatically be placed in volumes in the repository, making these persistent if the docker image is restarted or even destroyed

To successfully use the site, an admin account will need to be created, to do this:
1. Create a file in the root of the repository called `createaccount.env`
2. Fill in this file based on the template 
```
username=<FILL-IN>
password=<FILL-IN>
nickname=<FILL-IN>
```
`username` is the name used to log in to the site; `nickname` is the name shown when you are the author of a blog.

3. Run `docker-compose up` as before, a log should be shown, with the insert statement shown towards the end, just after any database migrations.
4. **IMPORTANT** - delete the `createaccount.env` file and/or change the password of the account as soon as you can successfully log in to the site.
5. You should now have an admin account. There is currently no way of creating additional users (non-admins cannot do anything with an account other than login and edit their profile anyway). The blog is set up to support multiple authors and show the name of the author at the start of the post.
